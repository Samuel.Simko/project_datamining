\documentclass{article}



\usepackage{arxiv}

\usepackage[utf8]{inputenc} % allow utf-8 input
\usepackage[T1]{fontenc}    % use 8-bit T1 fonts
\usepackage{hyperref}       % hyperlinks
\usepackage{url}            % simple URL typesetting
\usepackage{booktabs}       % professional-quality tables
\usepackage{amsfonts}       % blackboard math symbols
\usepackage{nicefrac}       % compact symbols for 1/2, etc.
\usepackage{microtype}      % microtypography
\usepackage{lipsum}		% Can be removed after putting your text content
\usepackage{graphicx}
\usepackage{natbib}
\usepackage{doi}



\title{Electronic Energy Prediction methods from molecular data}

%\date{September 9, 1985}	% Here you can change the date presented in the paper title
%\date{} 					% Or removing it

\author{Malik Algelly \\
	Department of Computer Science\\
	University of Geneva\\
	Geneva, Switzerland \\
	\texttt{Malik.Algelly@etu.unige.ch} \\
	%% examples of more authors
	\And
	Samuel Šimko \\
	Department of Computer Science\\
	University of Geneva\\
	Geneva, Switzerland \\
	\texttt{Samuel.Simko@etu.unige.ch} \\
}

% Uncomment to remove the date
%\date{}

% Uncomment to override  the `A preprint' in the header
\renewcommand{\headeright}{Technical Report}
\renewcommand{\undertitle}{Technical Report}
\renewcommand{\shorttitle}{Electronic Energy Prediction methods from molecular data}

%%% Add PDF metadata to help others organize their library
%%% Once the PDF is generated, you can check the metadata with
%%% $ pdfinfo template.pdf
\hypersetup{
pdftitle={A template for the arxiv style},
pdfsubject={q-bio.NC, q-bio.QM},
pdfauthor={David S.~Hippocampus, Elias D.~Striatum},
pdfkeywords={First keyword, Second keyword, More},
}

\begin{document}
\maketitle

\begin{abstract}
	Energy prediction of molecules is a task which depends on reliable prediction models.
	We propose four different models to predict the energies of Molecules. We implement a Python
	Library called \textit{Malikule}, in which we propose a simple way to obtain useful molecular
	data from their SMILES encoding.
\end{abstract}

% keywords can be removed
\keywords{Molecular Energy Prediction \and Data Mining \and Neural Networks}

\section{Introduction}


\section{Methodology}

We will first try a simple way of representing the data.
We will tokenize the SMILES string by calculating the SMILES Alphabet,
and create a feature per character in the alphabet, which will represent the number
of times this character appears in the SMILES representation of the molecule.

We will split our dataset in two different datasets:

\begin{itemize}
	\item The training dataset, on which we will train our model / validate our choices of hyperparameters for every model;
	\item The testing dataset, which we will keep independant with our training / validation process. We will only evaluate
		each algorithm once on this testing dataset, using the best hyperparameters we found on the training set.
\end{itemize}

We will use a simple linear algorithm as our baseline, then we will implement and
explain multiple other algorithms.

We will first find the best hyperparameters for all our different algorithms, using the training set.

This will be achieved using the library \textit{Optuna}, which is a highly optimized hyperparameter optimization framework.
For every model, we will define an objective function to optimize. The objective function takes as input a choice of hyperparameters,
and yields as output the performance of the model with these parameters. The \textit{Optuna} library then chooses intelligent
hyperparameters choices with an efficient algorithm. After doing a certain number of trials, the hyperparameters that obtained
the smallest loss will be used to evaluate the performance of the model on the testing set.

To determine the performance of a choice of hyperparameters in the Optuna objective function, we use KFold cross-validation to split our dataset into 5 folds.
We train 5 models using a different fold as validation set every time. We then keep the parameters of the model which
provided the best validation loss. We average the best validation losses obtained in the 5 models to get an estimate of the algorithm's performance
with these hyperparameters.

Finally, we will calculate each algorithm's performance on the testing dataset, and we will refute or validate our hypotheses.

\section{Data analysis pipeline}

\subsection{Dataset}

The dataset consists of $x$ molecules, represented by their SMILES string,
and different other attributes (Gibbs Energy, HOMO energy, \ldots).

Our objective is to predict the electronic energy of the coordinates (Energy\_) and the free corrected
energy at room temperature (Energy\_DG)

\subsection{Pre-processing}

The SMILES string contain information about the structure of the molecule.
For example, it contains the atoms the molecule contains. We will at first
try to extract simple information from this string for our baseline.
If we do not take into account the relation between the symbols in the string,
we can simply create one new feature, representing the number of times this symbol
appears in the string, for each symbol present in all strings.

For more complex algorithms, we want to keep the relationship between the symbols
as they appear in the string. We can therefore transform the string using one-hot vector encoding.
For deep learning algorithms, we hope this representation may be able to pick up useful information
otherwise lost in the other tokenization.

Looking at the dataset, we found that some data had NaN values in their attribute.
In order to solve this problem, we decided to delete all rows with NaN values.

Then, as requested, we kept one sample of each molecule with the lowest energy (Energy\_).

In addition, we have tokenize the SMILES string as explained above.

After that, in order to evaluate the importance of the attributes, we created three different datasets.
The first one contains no information about the composition of the molecule.
The second one contains only the information about the composition of the molecule.
And the last one contains all the available information.


\section{Descriptions of the algorithms used}
\subsection{Baseline algorithm: Linear Regression}
For our baseline, we chose the classical linear regression model.

Linear regression tries to find the linear relationship between two variables by calculating a linear equation.
If a linear relationship exists between the two variables, this equation allows us to estimate the desired variable.

The general linear equation is as follows: \[ Y = w^{T}X + b \]

In order to find the coefficients $w$ and the intercept $b$, we must minimize the residual
sum of squares between $Y$ and $w^{T}X$ as followed: \[ \min_{w} \sum_{n=1}^{n} (y_{i} - w^{T}x_{i})^{2} \]

This kind of method in linear regression is called an Ordinary least squares (OLS).

For this model, we used the Sklearn library which proposes an implementation of linear regression with the LinearRegression function.
In addition, we also used Sklearn to perform cross-validation using the KFold function.

This function allows us to separate the data set into several subsets in order to perform the cross-validation.
Before that, we split the dataset in two parts in order to have an unused set to test the best model found during the cross-validation.

\subsection{Support Vector Regression}

Support Vector Regression (SVR) is a specification of the Support Vector Machine (SVM) classification model.
In order to understand the SVR model, one must first understand the SVM model.
SVM is a model for binary classification of data. This model is used to find a hyperplane that best separates data.
The constraints ensure that the data is classified correctly. In other words, this problem consists in finding a hyperplane separating
the data and having the largest margin. It can be shown that maximizing the size of the margin is equivalent to minimizing the norm of the hyperplane vector.

More formally, SVM consists in solving the following minimization problem:

$$\min_{w} \frac{1}{2} \|w\|^{2}$$

with the constraints:

$$ y_{i}(\vec{w}\cdot\vec{x_{i}} - b) \geq 1 $$

where:\\
$y_{i} \in \{-1,1\}$ is the binary labels\\
$\vec{w}$ is the hyperplane\\
$\vec{x_{i}}$ is the $i^{th}$ data\\
$b$ is the intercept\\

This problem can be expressed using the Lagrange multiplier as follows:
$$ L( w, b, \lambda) = \frac{1}{2} w^{T} w - \sum_{i=1}^{N} \lambda_{i}(y_{i}(w^{T}x_{i}-b)-1)$$

By a derivation sequence, this equation can be expressed as follows:
$$ L(\lambda) = -\frac{1}{2} \sum_{i=1}^{N} (\sum_{j=1}^{N} \lambda_{i} \cdot \lambda_{j}\cdot y_{i}\cdot y_{j}\cdot \vec{x}_{i}^{\,T} \cdot\vec{x}_{j})
	+ \sum_{i=1}^{N}\lambda_{i}
$$
with contraints $\lambda_{i} \geq 0$

This form allows us to introduce the kernel trick. Originally, SVM is used to classify linearly separable data. However, thanks to the so-called kernel trick,
it is possible to adapt the model to non-linearly separable data. This kernel trick allows us to perform a transformation of the data in higher dimensions
where the data are linearly separable.

By a mathematical trick, the kernel trick allows to calculate the dot product between two transformed vectors without actually
transforming each vector. This trick transforms the previous equation into the following:
$$ L(\lambda) = -\frac{1}{2} \sum_{i=1}^{N} (\sum_{j=1}^{N} \lambda_{i} \cdot \lambda_{j}\cdot y_{i}\cdot y_{j}\cdot K(\vec{x}_{i} \cdot\vec{x}_{j}))
	+ \sum_{i=1}^{N}\lambda_{i}
$$

We arrive at the following problem:

$$ \min_{\lambda_{i} \geq 0 }  \frac{1}{2} \sum_{i=1}^{N} (\sum_{j=1}^{N} \lambda_{i} \cdot \lambda_{j}\cdot y_{i}\cdot y_{j}\cdot K(\vec{x}_{i} \cdot\vec{x}_{j}))
	- \sum_{i=1}^{N}\lambda_{i}
$$

Let's talk about SVR.

Unlike SVM, SVR seeks to predict the continuous values of a so-called dependent variable. Moreover, for SVR, the size of the margin is a hyperparameter.
This model will perform the linear regression by trying to minimize the number and distance of data falling outside the margin.

Another hyperparameter $C$ allows us to weight the importance of the data falling outside the margin.

The objective function of SVR is as follows: $$ \min_{w} \frac{1}{2} \|w\|^{2} + C \sum_{i=1}^{n}|\xi_{i}| $$
And the constraints of the problem is: $$ |y_{i} - w_{i}x_{i}| \leq \epsilon + |\xi_{i}| $$

where:\\
$y_{i}$ is the continuous values that we are trying to predict\\
$\vec{w}$ is the hyperplane\\
$\vec{x_{i}}$ is the $i^{th}$ data\\
$\epsilon$ is the size of the margin\\
$\xi_{i}$ is the distance outside the margin of the ith data\\
$C$ is the weighting of the distances of the data falling outside the margin

Sklearn uses the following kernel called RBF:
$$K(\vec{x}_{i},\cdot\vec{x}_{j}) = \exp^{-\gamma \|\vec{x}_{i}-\vec{x}_{j}\|^{2}}$$

To summarize the role of hyperparameters:\\
$\epsilon$ is the tolerance margin we set for the prediction values.\\
$C$ is the coefficient representing the importance we want to give to the data falling outside the margin.\\
$\gamma$ is the polynomial coefficient of the prediction\\

We have optimized these hyperparameters with the optuna library. Similar to linear regression,
we used Sklearn's KFold function to perform cross-validation of our model.

\subsection{Multi-Layered Perceptrons}

Multi-Layered Perceptrons (MLP) are the cornerstone of Deep Neural Networks.
We propose a PyTorch (\cite{NEURIPS2019_9015}) implementation of MLPs.
We use the PyTorch-Lightning library (\cite{william_falcon_2020_3828935}) to organize our data processing pipeline, our cross-validation
algorithm, and our training, validation and testing datasets and dataloaders in a single datamodule.

We represent using TensorBoard an example of a Multi-Layered Perceptron in Figure \ref{fig:rnn}

\begin{figure}[htpb]
	\centering
	\includegraphics[width=0.8\textwidth]{images/lstm.png}
	\caption{An example of the Multi-Layered Perceptron (MLP) we have created. This network
		takes all available inputs, passes through two linear layers and two ReLU activation functions.}
	\label{fig:rnn}
\end{figure}

The hyperparameters we will optimize are the number of hidden layers, the number of perceptrons in
a single layer, the activation function to use (ReLU, SeLU\ldots), the learning rate, the $\gamma$ parameter
of the exponential scheduler used, and the batch size. We use the Optuna library to find the best hyperparameters.
We will not consider networks with just one single linear layer as this type of network is a linear transform.

\subsection{Recurrent Neural Networks}

Recurrent Neural Networks (RNNs) are simple and effective networks in natural language processing.
We will use a RNN on the SMILES representation of the string. Since RNNs are known to suffer from short-term
memory, we will also use the PyTorch LSTM layer to compare it to the simple RNN layer.

We represent using TensorBoard an example of Recurrent Neural Network in Figure \ref{fig:rnn}

\begin{figure}[htpb]
	\centering
	\includegraphics[width=0.8\textwidth]{images/lstm.png}
	\caption{An example of the Recurrent Neural Network (RNN) we have implemented. The SMILES one hot encoded vector is fed to a LSTM layer.
	The other features are fed to a MLP. Finally, both outputs are concatenated and fed to a final MLP layer.}
	\label{fig:rnn}
\end{figure}

\section{Results}

\subsection{Baseline: Linear Regression}

We performed the linear regression of the different data sets we prepared earlier.
As we can see on the figure \ref{table_reg}, the dataset with the attributes containing the information
on the composition of the molecules, gives us better results than the one without this information.

\begin{figure}[h!]
	\centering
	\caption{Result linear regression}
	\label{table_reg}
	\begin{tabular}{|c | c | c|}
	\hline
	 Dataset & MSE Energy & MSE Energy DG\\
	 \hline
	 without SMILES & 0.2103 & 0.2109\\
	 \hline
	 with only SMILES & 0.00032 & 0.00030\\
	 \hline
	 with all & 2.634e-06 & 2.642e-06\\
	 \hline
	\end{tabular}
\end{figure}

These results led us to believe that the energy predictions depended mainly on the compositional attributes of the molecules.
Moreover, given the MSE, this would seem to suggest that the relationship between attributes and energy is linear.

In order to further investigate these initial observations, we decided to use the linear regression model with Lasso regularization.
This model should allow us to observe the most important attributes for the predictions by setting to 0 the weights corresponding
to the least useful attributes.

\subsection{Lasso Linear Regression}

The Lasso model allowed us to confirm the importance of the attributes concerning the composition of the molecules.

In order to find the best $\alpha$ hyperparameter, we used cross-validation and the Optuna library as explained above.

We can mention here the fact that the Lasso regression model with an $\alpha$ parameter equal to 0 corresponds to a simple linear regression.

The figure \ref{fig:Lasso1} tends to confirm the first assumptions of linearity of the data.
We can see that the smaller the $\alpha$ parameter, the better the MSE results.

\begin{figure}[htpb]
	\centering
	\includegraphics[width=1\textwidth]{images/Lasso_X_with_SMILES_energy_50.png}
	\caption{Optimization of the Lasso model with the set containing all the information for Energy prediction. 100 trials for $\alpha$ ranging from 0.001 to 10}
	\label{fig:Lasso1}
\end{figure}


Let us now analyze the model by setting the $\alpha$ parameter to 0.001.

By setting the $\alpha$ parameter to 0.001, we obtain an MSE of 6.69e-05 for the prediction of Energy and Energy\_DG.
Moreover, by observing the weights, we can see that on the 50 weights, only 22 are not at 0 for the prediction of Energy
and 21 for the prediction of Energy\_DG.

If we look deeper, we can see that only one of these weights corresponds to an attribute that is not on the composition of the molecule.
These results tend to confirm the hypothesis of the primary importance of the molecule's composition for the prediction of its energy

\subsection{Support Vector Regression}

The goal now is to provide more evidence regarding the linear relationship of the data.
To do this, we decided to test the data on a non-linear model.
We therefore used an SVR model with an RBF kernel.

First, we launched the optimization of the model with large value intervals in order to have initial observations.
We can see on the figure \ref{fig:SVR1} that the model gives better results for $\gamma$ and $\epsilon$ values close to 0.
So we decided to change the range of values for $\gamma$ and $\epsilon$.
The figures \ref{fig:SVR2} and \ref{fig:SVR3} confirm our first observations on the optimal value interval for epsilon and gamma.

Finally, by setting $C$ to 69, $\epsilon$ to 0.01 and $\gamma$ to 0.0001,
we obtain an MSE of 7.27e-05 for the prediction of Energy and 7.31e-05 for Energy\_DG.

The assumption of data linearity explains the value of these hyperparameters.
Indeed, the SVR model approaches a linear regression if epsilon and gamma tend towards 0.
Moreover, the more the parameter C increases, the more the data out of the margin will be taken into account,
and thus the more the hyperplane will minimize the distances of the data out of the margin.
It is a linear regression.

\begin{figure}[htpb]
	\centering
	\includegraphics[width=0.8\textwidth]{images/SVR_X with SMILES_energy_50.png}
	\caption{Optimization of the SVR model with the set containing all the information for Energy prediction.
	1000 trials for $C$ from 1 to 100, $\epsilon$ from 0 to 1 and $\gamma$ from 0 to 10}
	\label{fig:SVR1}
\end{figure}


\begin{figure}[htpb]
	\centering
	\includegraphics[width=0.8\textwidth]{images/SVR_X with SMILES_energy_50_2.png}
	\caption{Optimization of the SVR model with the set containing all the information for Energy prediction.
	100 trials for $C$ from 1 to 100, $\epsilon$ from 0 to 0.2 and $\gamma$ from 0 to 1}
	\label{fig:SVR2}
\end{figure}

\begin{figure}[htpb]
	\centering
	\includegraphics[width=0.8\textwidth]{images/SVR_X with SMILES_energy_50_3.png}
	\caption{Optimization of the SVR model with the set containing all the information for Energy prediction.
	20 trials for $C$ from 1 to 100, $\epsilon$ from 0 to 0.02 and $\gamma$ from 0 to 0.001}
	\label{fig:SVR3}
\end{figure}

\subsection{Multi-Layered Perceptrons}

Even though we have developed the full Optuna hyperparameter optimization framework, due to time constraints and unavailable material, we have not launched a full Optuna hyperparameter analysis on our models.
To find good hyperparameters, we tested several values by hand.

For the MLP, we found that using a low number of layers, such as 2 layers, and a sufficient number of perceptrons in each layer (more than 10)
yields the best results. We found that using too deep of a network makes the training curves erratic. We tried adding Batch Normalization layers
and Dropout layers to have a more stable network and to be able to use more layers, but both these approaches did not give better results than using just 2 layers.

We thus stop our hyperparameter search with an MLP of two layers and 20 perceptrons per layer, with a best validation loss of 0.004.
Figure \ref{fig:loss} shows the training and validation curves of the MLP network for the first fold of the KFold process. We see that
the network has overfitted: the training loss is lower than 8e-6, but the validation loss encountered a plateau at 1e-3.

We expect that a full hyperparameter search would have provided better results.

\begin{figure}[htpb]
	\centering
	\includegraphics[width=0.5\textwidth]{images/loss.png}
	\caption{The training and validation curves for the first fold of our MLP network. We see that the network has overfitted.}
	\label{fig:loss}
\end{figure}


\subsection{R\N}

For the RNN, we found that using a LSTM cell instead of a RNN cell greatly increases the performance of the validation set (from 4.0e-3 to 8.0e-5).


\subsection{Comparing our algorithms together}
\begin{figure}[htpb]
	\centering
	\begin{tabular}{|c|c|c|}
		\hline
		Algorithm & Testing loss \\
		\hline
		Linear Regression (Baseline) & 2.64e-06 \\
		\hline
		SVR & 4.78e-05 \\
		\hline
		MLP & 7.09e-04 \\
		\hline
		RNN & 6.39e-04 \\
		\hline
	\end{tabular}
	\caption{Validation and testing losses for each algorithm used}
	\label{fig:tabloss}
\end{figure}

In Figure \ref{fig:tabloss}, we plot the testing loss achieved for each algorithm.
For each algorithm, the hyperparameters used were the result of the Optuna optimization
on cross-validation.

We see in the Figure \ref{fig:tabloss} that the Linear regression performed the best out of all our models.

To see We will perform a hypothesis test in order to figure out if the models achieved perform the same as the
baseline.

We note $H_0$ the null hypotheses (The model and Linear Regression are equal in performance),
and $H_A$ the alternative hypothesis (The model and Linear Regression are not equal in preformance).

We will do a paired sample t-test. In order to do so, we make the following assumptions:

\begin{itemize}
	\item The observations of the dataset are independant with each other;
	\item The dependent variables (\textit{Energy\_}, and \textit{Energy\_DG}) are close to being normally
		distributed. This seems to be the case if we plot the histogram (Figure \ref{hist});
\end{itemize}

We use a level of significance of $\alpha = 0.05$. The paired t-test will tell us if the mean loss
of the two models are the same.

For the SVR test predictions, we get a p-value of 2.178e-05 for the Energy\_ attribute,
while 0.1766. As the p-value is above the level
of significance, we can reject the null hypothesis.

For the lasso test predictions, we get a p-value of 0.040.
As the p-value is below the level
of significance, We cannot reject the null hypothesis.

For the MLP, we get a p-value of 0.13 for both dependent variables. We can reject the null
hypothesis.

For the Reccurent Neural Network, we get a p-value of 0.857 for the Energy\_ attribute,
and a p-value of 0.598 for the Energy\_DG attribute. We can reject the null hypothesis.

As we did not find evidence which points to the hypothesis that the label is more than just linear,
we apply Occam's razor and conclude that the simplest model is to be preferred.
In more complex databases, we would use linear algorithms such as Linear Regression or Support Vector Machines
with a linear kernel.

\section{Conclusion}

We have successfully designed, implemented, and tested four different algorithms
on the molecular dataset. We obtain similar losses with all our algorithms.
We have studied our dataset to find several different ways of representing our data.
After introducing key concepts of the algorithms implemented, we found the best hyperpamaters
for each of the algorithms using KFold cross-validation and Optuna to perform an intelligent grid-search.

The baseline algorithm, a linear regression, performs extremely well with the right SMILES encoding.
We compared the best models for each different algorithm on the testing dataset. We found that linear regression
is competitive with the other methods we used.

We use Occam's razor to determine that a simple linear regression with encoding of the number of occurences
of the different symbols of the SMILES string is to be preferred for practical applications
of molecular energy prediction.

\section{Acknowledgments}

We thank the core staff of the \textit{Data Mining} course of the University of Geneva
for their help.

We acknowledge the use of Scikit-Learn functions (\cite{https://doi.org/10.48550/arxiv.1201.0490}).

\bibliographystyle{unsrtnat}
\bibliography{references}  %%% Uncomment this line and comment out the ``thebibliography'' section below to use the external .bib file (using bibtex) .
\end{document}

