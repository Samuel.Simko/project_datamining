.. maliule.models

.. currentmodule:: malikule.models

Models subpackage (malikule.models)
========================================

.. autosummary::
   :toctree: generated/

   RNN
   MLP
