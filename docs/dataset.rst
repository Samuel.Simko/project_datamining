.. maliule.dataset

.. currentmodule:: malikule.dataset

Dataset subpackage (malikule.dataset)
========================================

.. autosummary::
   :toctree: generated/

   MoleculeDataModule
