.. Malikule documentation master file, created by
   sphinx-quickstart on Tue Mar 30 10:12:14 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Malikule's documentation!
====================================

`Malikule` is a python library designed to train and test
simple artificial intelligence models on molecular energy prediction.

It was created as a project for the 2022 Data Mining course
of the University of Geneva.

`Malikule` supports Linear Regression,
Support Vector Machines, Multi-Layered Perceptrons
and Recurrent Neural Networks.

Installation
------------

`Malikule` can be installed with `pip`:

.. code::

   $ cd malikule
   $ pip install .

It is recommended to work in a virtual environment.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Documentation
-------------

.. toctree::
   :maxdepth: 2

   dataset
   models

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
