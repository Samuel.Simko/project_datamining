import torch
import optuna

import torch.nn as nn
import numpy as np
import torch.nn.functional as F
import pytorch_lightning as pl

from malikule.dataset import MoleculeDataModule


# Define network model
class TestModel(pl.LightningModule):
    def __init__(self, num_features, lr):
        super().__init__()
        # Define loss function and learning rate
        self.loss = F.mse_loss
        self.lr = lr

        # Define net
        self.net = nn.Sequential(
            nn.Flatten(),
            nn.Linear(num_features, 2),
        )

    def forward(self, x):
        x = self.net(x)
        return x

    def training_step(self, batch, batch_idx):
        x, y = batch
        y_hat = self.forward(x)
        loss = self.loss(y, y_hat)
        self.log("train_loss", loss)
        return {"loss": loss}

    def validation_step(self, batch, batch_idx):
        x, y = batch
        y_hat = self.forward(x)
        loss = self.loss(y, y_hat)
        self.log("val_loss", loss)
        return {"val_loss": loss}

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=self.lr)
        return optimizer


class BestValidationLossCallback(pl.Callback):
    """Callback to store the best validation loss when training a model"""

    def __init__(self):
        super().__init__()
        self.bestval = np.infty

    def on_validation_epoch_end(self, trainer, pl_module):
        val_loss = trainer.callback_metrics["val_loss"]
        if val_loss < self.bestval:
            self.bestval = val_loss


def objective(trial):
    """Objective function to minimize in Optuna"""

    lr = trial.suggest_loguniform("learning_rate", 1e-4, 1e-1)
    losses = []
    n_splits = 2

    for i in range(n_splits):
        dm = MoleculeDataModule(
            "../data/data.csv",
            use_kfold=True,
            use_molvecgen=False,
            batch_size=64,
            n_splits=n_splits,
        )
        dm.setup_folds(fold_index=i)
        model = TestModel(num_features=dm.num_features, lr=lr)
        best_val_loss_callback = BestValidationLossCallback()
        trainer = pl.Trainer(max_epochs=10, callbacks=[best_val_loss_callback])
        trainer.fit(model, datamodule=dm)
        losses.append(best_val_loss_callback.bestval)

    print("Average loss of trial:", np.mean(losses))
    return np.mean(losses)


study = optuna.create_study()
study.optimize(objective, n_trials=4)

print(study.best_params)
