import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import KFold

class MoleculeSklearnDataset():
    def __init__(self, path_of_csv):
        self.data = pd.read_csv(path_of_csv)

        self.data = self.data.dropna()

        self.data = self.data.sort_values(by=["Energy_(kcal/mol)"])
        self.data = self.data.drop_duplicates(subset=["Chiral_Molecular_SMILES"])
        self.data = self.data.sort_index()

        # Make attributes consisting of the number of occurences
        # of characters in the Chiral Molecular SMILES, for all characters
        # possible. (This is a very simple tokenization)
        symbols = np.unique(list("".join(self.data["Chiral_Molecular_SMILES"])))
        symbols.sort()
        for symbol in symbols:
            if symbol in list("""\\^$.|?*+()[]\\{\\}"""):
                self.data["Count of {}".format(symbol)] = self.data[
                    "Chiral_Molecular_SMILES"
                ].str.count("\\" + symbol)
            else:
                self.data["Count of {}".format(symbol)] = self.data[
                    "Chiral_Molecular_SMILES"
                ].str.count(symbol)

        self.X = self.data.drop(
            columns=[
                "Energy DG:kcal/mol)",
                "Energy_(kcal/mol)",
                "Chiral_Molecular_SMILES",
                ]
            )

        self.y = self.data[["Energy DG:kcal/mol)", "Energy_(kcal/mol)"]]


        self.X = (self.X - self.X.mean()) / self.X.std()
        self.y = (self.y - self.y.mean()) / self.y.std()


    def getDataNoSmiles(self):
        cols = [c for c in self.X.columns if c.lower()[0:5] != 'count']
        return self.X[cols]

    def getDataWithOnlySmiles(self):
        cols = [c for c in self.X.columns if c.lower()[0:5] == 'count']
        return self.X[cols]

    def getDataWithSmiles(self):
        return self.X

    def getY(self):
        return self.y
